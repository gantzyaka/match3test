﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Match3Types;

/// <summary>
/// The main control script of the game.
/// </summary>
public class GameManager : MonoBehaviour, IMatch3GameHandler
{
    //Singleton instance.
    public static GameManager Inst { get; private set; }

    public GameObject UnitPrefab;
    public Transform MapTrans;
    public Text ScoreText;
    public TextTweener EarnedScoreText;

    [Header("Map Size")]
    public int Rows;
    public int Columns;

    [Header("Variation")]
    public int ColorsCount;

    [Header("Unit Options")]
    public float UnitSize = 50f;
    public float UnitSpeed = 200f;

    private Match3Game Game { get; set; }
    private UnitInfo[,] UnitsArray { get; set; } //Array of links to unit management scripts.
    private Color[] Colors { get; set; } //Colors for units, assigned by id.
    private UnitInfo SelectedUnit { get; set; } //Selected by player current unit.
    private GameState State { get; set; } = GameState.None;

    //Lists of units that need to be moved in FixedUpdate.
    private List<UnitInfo> UnitsToMove { get; set; } = new List<UnitInfo>();    
    private List<UnitInfo> UnitsToDieAndReborn { get; set; } = new List<UnitInfo>();

    //Coordinates where changes occurred in the units, and where we need to check for new matches.
    private List<Pos> UnitsPossToCheckMatches { get; set; } = new List<Pos>(); 


    private void Awake()
    {
        //Singleton implementation.
        if (Inst == null)
            Inst = this;
        else if (Inst != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Validation of the entered data.
    /// </summary>
    private void OnValidate()
    {
        if (Rows < 3)
            Rows = 3;

        if (Columns < 3)
            Columns = 3;

        if (ColorsCount < 3)
            ColorsCount = 3;

        if (UnitSize < 0)
            UnitSize = 1f;

        if (UnitSpeed < 0)
            UnitSpeed = 1f;
    }

    private void Start()
    {
        //Shift the playing field to the center of the screen.
        MapTrans.localPosition = new Vector3(Columns * -1 * UnitSize / 2f, Rows * -1 * UnitSize / 2f, 0f);

        if (Colors == null)
            SetupColors();

        InitNewGame();
    }

    /// <summary>
    /// Creating an array of random colors for units.
    /// </summary>
    private void SetupColors()
    {
        Colors = new Color[ColorsCount];

        for (int i = 0; i < ColorsCount; i++)
            Colors[i] = new Color(Random.value, Random.value, Random.value, 1.0f);
    }

    /// <summary>
    /// Creating a new Match3-game.
    /// </summary>
    private void InitNewGame()
    {        
        Game?.Dispose();
        Game = new Match3Game(Rows, Columns, ColorsCount, this);
        ScoreText.text = "0";
        EarnedScoreText.UpdateText("", withMoving: false);
    }

    /// <summary>
    /// Accept new or updated game map after it has been built and update units according to the map.
    /// </summary>
    /// <param name="map"></param>
    public void MapUpdateHandler(int[,] map)
    {
        if (map == null) return;

        int x = map.GetLength(0);
        int y = map.GetLength(1);

        if (UnitsArray == null)
            UnitsArray = new UnitInfo[x, y];

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                int id = map[i, j];

                if (id == -1)
                {
                    if (UnitsArray[i, j] != null)
                        Destroy(UnitsArray[i, j].gameObject);
                }
                else
                {
                    if (UnitsArray[i, j] == null)
                    {
                        GameObject go = Instantiate(Inst.UnitPrefab, Vector3.zero, Quaternion.identity, MapTrans);
                        UnitsArray[i, j] = go.GetComponent<UnitInfo>();
                        UnitsArray[i, j].InitUnit(new Pos(i, j), UnitSize);
                    }

                    UnitsArray[i, j].UpdateUnitIdAndColor(id, Colors[id]);
                }
            }
        }
    }

    /// <summary>
    /// Swap of two units.
    /// </summary>
    /// <param name="units"></param>
    private void SwapTwoUnits(params UnitInfo[] units)
    {
        if (units == null || units.Length != 2) return;

        State = GameState.Swap;

        for (int i = 0; i < 2; i++)
        {
            UnitsToMove.Add(units[i]);
            UnitsPossToCheckMatches.Add(units[i].MPos);

            //Replace links for swapped units in the common array of units.
            UnitsArray[units[i].MPos.X, units[i].MPos.Y] = units[i == 0 ? 1 : 0];
        }

        //Replace positions for swapped units.
        Pos temp = units[0].MPos;
        units[0].SetNewPosition(units[1].MPos);
        units[1].SetNewPosition(temp);
    }

    /// <summary>
    /// Player's selection of units.
    /// In the case of the selection of the second unit, check the possibility of their swapping. If it leads to the match, swap selected units.
    /// </summary>
    /// <param name="ci"></param>
    public void UnitClickHandler(UnitInfo ci)
    {
        if (ci == null || State != GameState.None) return;

        if (SelectedUnit == null)
        {
            SelectedUnit = ci;
            SelectedUnit.ShowShadow();
        }
        else
        {
            if (SelectedUnit != ci && Game.TrySwapUnits(SelectedUnit.MPos, ci.MPos))
                SwapTwoUnits(SelectedUnit, ci);

            SelectedUnit.HideShadow();
            SelectedUnit = null;
        }
    }

    /// <summary>
    /// Performing all unit movements, according to the formed lists of units that need to be moved (UnitsToMove, UnitsToDieAndReborn).
    /// </summary>
    private void FixedUpdate()
    {
        bool finished = true;
        List<UnitInfo> units = null;

        switch (State)
        {
            case GameState.Swap:
            case GameState.Fall:
                units = UnitsToMove; break;

            case GameState.Burn:
            case GameState.Fill:
                units = UnitsToDieAndReborn; break;
        }

        if (units == null) return;

        foreach (UnitInfo unit in units)
            if (unit.DoAction(State, UnitSpeed))
                finished = false;

        if (finished) //The game states switch only after all movements of the current state are completed.
            NextGameState();
    }

    /// <summary>
    /// Delay before the destroing of the matched units.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ShowMatchAndWait()
    {
        yield return new WaitForSeconds(0.5f);
        State = GameState.Burn;
    }

    /// <summary>
    /// Transition between game states.
    /// </summary>
    private void NextGameState()
    {
        switch (State)
        {
            case GameState.Swap:
                if (CheckForMatches())
                {
                    StartCoroutine(ShowMatchAndWait());
                    State = GameState.ShowMatch;
                }
                else
                    State = GameState.None;
                break;

            case GameState.Burn:
                State = GameState.Fall;
                foreach (UnitInfo unit in UnitsToDieAndReborn)
                    unit.Reborn();
                break;

            case GameState.Fall:
                State = GameState.Fill;
                foreach (UnitInfo unit in UnitsToDieAndReborn)
                    unit.ShowUnit();                
                break;

            case GameState.Fill:
                UnitsToDieAndReborn.Clear();
                if (CheckForMatches())
                {
                    StartCoroutine(ShowMatchAndWait());
                    State = GameState.ShowMatch;
                }
                else
                    State = GameState.None;
                break;
        }
    }

    /// <summary>
    /// Check for matches.
    /// If successful, start displaying the received changes in the units, by performing sequentially game states.
    /// </summary>
    /// <returns></returns>
    private bool CheckForMatches()
    {
        //Exit if there are no places to check new matches.
        if (UnitsPossToCheckMatches.Count == 0) return false;

        //Received changes in the units.
        List<Pos> matchedUnits;
        List<PosVector> fallingUnits;
        List<UnitData> newUnits;

        Game.DoMatchesPipeline(out matchedUnits, out fallingUnits, out newUnits, UnitsPossToCheckMatches.ToArray());

        UnitsToMove.Clear();
        UnitsPossToCheckMatches.Clear();

        //Exit if there are no new matches found.
        if (matchedUnits == null || matchedUnits.Count == 0) return false;

        //We keep the places of the matched units and units that are going to fall, to check them for new matches in the future.
        foreach (PosVector posv in fallingUnits)
            UnitsPossToCheckMatches.Add(posv.From);
        foreach (Pos pos in matchedUnits)
            UnitsPossToCheckMatches.Add(pos);

        //Falling units keep their target positions and are added to the list of units to be moved.
        foreach (PosVector pos in fallingUnits)
        {
            UnitInfo unit = UnitsArray[pos.From.X, pos.From.Y];
            unit.SetNewPosition(pos.To);
            UnitsToMove.Add(unit);
        }

        //Matched units are highlighted and take on new positions and colors for rebirth. And they are added to the list of units to be moved (die and reborn).
        for (int i = 0; i < newUnits.Count; i++)
        {
            Pos pos = matchedUnits[i];
            UnitInfo unit = UnitsArray[pos.X, pos.Y];
            unit.SetNewPosition(newUnits[i].Pos);
            unit.SetNewUnitIdAndColor(newUnits[i].Id, Colors[newUnits[i].Id]);
            unit.ShowShadow();
            UnitsToDieAndReborn.Add(unit);
        }

        //Replacing links to units according to their new positions (in which they have to be after their movements).
        foreach (UnitInfo unit in UnitsToDieAndReborn)
            UnitsArray[unit.MPos.X, unit.MPos.Y] = unit;

        foreach (UnitInfo unit in UnitsToMove)
            UnitsArray[unit.MPos.X, unit.MPos.Y] = unit;

        return true;
    }

    /// <summary>
    /// Accept updates of earned points.
    /// </summary>
    /// <param name="currentScore"></param>
    /// <param name="earnedScore"></param>
    public void ScoreUpdateHandler(int currentScore, int earnedScore)
    {
        ScoreText.text = currentScore.ToString();
        EarnedScoreText.UpdateText("+" + earnedScore);
    }

    /// <summary>
    /// Reset progress and start a new game.
    /// </summary>
    public void ResetGame()
    {
        InitNewGame();
    }

    /// <summary>
    /// Creating a new array of random colors and applying it for units.
    /// </summary>
    public void ResetColors()
    {
        SetupColors();

        foreach (UnitInfo unit in UnitsArray)
            unit.UpdateUnitColor(Colors[unit.Id]);
    }
}
    
